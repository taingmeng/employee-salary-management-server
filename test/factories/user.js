const { User } = require('../../server/models');

module.exports = (props = {}) => User.create(props);
