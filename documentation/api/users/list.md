# List Users

Return a list of users based on query parameters. If not query parameter, all users will be returned.

## Endpoint

```
GET /users
```

## Query Params

| key       | description | default |
| --------- | ----------- | ------- |
| limit     |             | 0       |
| offset    |             | 0       |
| minSalary |             | 0       |
| maxSalary |             | 0       |
| sort      |             | 0       |

## Response Body

```
{
    "count": 1000,
    "rows": [
        {
            "id": 65004,
            "employeeId": "b2032bf1-58b8-4b8a-bc59-3b9cb359c9a0",
            "username": "dgobourn0",
            "name": "Daphne Gobourn",
            "salary": 2097.99,
            "createdAt": "2020-05-31T09:10:22.000Z",
            "updatedAt": "2020-05-31T09:10:22.000Z"
        },
        ...
    ]
}
```

## Error Status Codes

| code | description                      |
| ---- | -------------------------------- |
| 400  | Query parameter validation error |
