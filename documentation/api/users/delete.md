# Delete User

Soft delete a user. This sets `deletedAt` to current date time.

## Endpoint

```
DELETE /users/:id
```

## Response Body

```
{}
```

## Error Status Codes

| code | description    |
| ---- | -------------- |
| 404  | User not found |
