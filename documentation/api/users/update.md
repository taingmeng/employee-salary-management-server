# Update User

Update the details of a user

## Endpoint

```
POST /users/:id
```

## Request Body

At least one of the following properties must be present.

| key        | description                          | required |
| ---------- | ------------------------------------ | -------- |
| employeeId | Unique ID used in the organization.  | No       |
| username   | Unique username of the user account. | No       |
| name       | Full name of the user.               | No       |
| salary     | Salary of the user.                  | No       |

```
{
	"employeeId": "e0001",
	"username": "taingmeng",
	"name": "Meng Taing",
	"salary": 5000
}
```

## Response Body

```
{}
```

## Error Status Codes

| code | description                   |
| ---- | ----------------------------- |
| 404  | User not found                |
| 400  | Request body validation error |
