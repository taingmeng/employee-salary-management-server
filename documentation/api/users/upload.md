# Upload Users

Upload CSV to bulk create users.

## Endpoint

```
POST /users/upload
```

## Request Form Data

| key   | description                                                   | required |
| ----- | ------------------------------------------------------------- | -------- |
| file  | Selected CSV file                                             | Yes      |
| delay | Simulate long processing time when large file is being upload | No       |

## Response Body

```
{
    "count": 1000
}
```

## Error Status Codes

| code | description                   |
| ---- | ----------------------------- |
| 400  | Request body validation error |
