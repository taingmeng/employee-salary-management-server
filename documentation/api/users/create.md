# Create User

Create a new user with details

## Endpoint

```
POST /users/:id
```

## Request Body

| key        | description                          | required |
| ---------- | ------------------------------------ | -------- |
| employeeId | Unique ID used in the organization.  | Yes      |
| username   | Unique username of the user account. | Yes      |
| name       | Full name of the user.               | Yes      |
| salary     | Salary of the user.                  | Yes      |

```
{
	"employeeId": "e0001",
	"username": "taingmeng",
	"name": "Meng Taing",
	"salary": 5000
}
```

## Response Body

```
{
    "id": 66004,
    "employeeId": "e0001",
    "username": "taingmeng",
    "name": "Meng Taing",
    "salary": 5000,
    "updatedAt": "2020-06-01T05:59:04.141Z",
    "createdAt": "2020-06-01T05:59:04.141Z"
}
```

## Error Status Codes

| code | description                   |
| ---- | ----------------------------- |
| 400  | Request body validation error |
