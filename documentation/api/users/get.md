# Get User

Return the details of a user.

## Endpoint

```
GET /users/:id
```

## Response Body

```
{
    "id": 65004,
    "employeeId": "b2032bf1-58b8-4b8a-bc59-3b9cb359c9a0",
    "username": "dgobourn0",
    "name": "Daphne Gobourn",
    "salary": 2097.99,
    "createdAt": "2020-05-31T09:10:22.000Z",
    "updatedAt": "2020-05-31T09:10:22.000Z"
}
```

## Error Status Codes

| code | description    |
| ---- | -------------- |
| 404  | User not found |
