const Joi = require('@hapi/joi');

module.exports = (req, res, next) => {
  const schema = Joi.object()
    .label('formData')
    .keys({
      delay: Joi.number().integer().positive().allow(0),
    });
  const { value, error } = schema.validate(req.body, {
    allowUnknown: false,
  });
  if (error) {
    next(error);
  } else {
    req.body = value;
    next();
  }
};
