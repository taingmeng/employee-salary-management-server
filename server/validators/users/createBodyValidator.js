const Joi = require('@hapi/joi');

const ONE_MILLION = 1000000;

const schema = Joi.object()
  .label('body')
  .keys({
    employeeId: Joi.string().required(),
    name: Joi.string().required(),
    username: Joi.string().required(),
    salary: Joi.number().precision(2).min(0).max(ONE_MILLION).required(),
  });

const validator = (req, res, next) => {
  const { value, error } = schema.validate(req.body, {
    allowUnknown: false,
  });
  if (error) {
    next(error);
  } else {
    req.body = value;
    next();
  }
};

validator.schema = schema;

module.exports = validator;
