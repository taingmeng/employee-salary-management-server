const Joi = require('@hapi/joi');

const ONE_MILLION = 1000000;

module.exports = (req, res, next) => {
  const schema = Joi.object()
    .label('body')
    .keys({
      employeeId: Joi.string(),
      name: Joi.string(),
      username: Joi.string(),
      salary: Joi.number().precision(2).min(0).max(ONE_MILLION),
    })
    .or('employeeId', 'name', 'username', 'salary');
  const { value, error } = schema.validate(req.body, {
    allowUnknown: false,
  });
  if (error) {
    next(error);
  } else {
    req.body = value;
    next();
  }
};
