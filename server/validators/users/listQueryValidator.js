const Joi = require('@hapi/joi');

module.exports = (req, res, next) => {
  const schema = Joi.object()
    .label('queryParams')
    .keys({
      limit: Joi.number().integer().positive().allow(0),
      offset: Joi.number().integer().positive().allow(0),
      page: Joi.number().integer().positive().default(0),
      maxSalary: Joi.number().positive(),
      minSalary: Joi.number().positive(),
      query: Joi.string(),
      sort: Joi.string().regex(/^[+-]\w+/),
    });
  const { value, error } = schema.validate(req.query);
  if (error) {
    next(error);
  } else {
    req.query = value;
    next();
  }
};
