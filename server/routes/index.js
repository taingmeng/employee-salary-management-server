const { Router } = require('express');
const apiRouter = require('./api');

const router = new Router();

router.get('/', (req, res) => {
  res.json({ ok: true });
});

router.use('/api', apiRouter);

module.exports = router;
