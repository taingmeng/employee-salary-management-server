const { Router } = require('express');
const set = require('lodash/set');
const get = require('lodash/get');
const csv = require('csv-parser');
const fs = require('fs');

const { Sequelize } = require('../../models');
const { User, Lock, sequelize } = require('../../models');
const {
  listQueryValidator,
  createBodyValidator,
  updateBodyValidator,
  uploadBodyValidator,
} = require('../../validators/users');
const asyncWrapper = require('../../middlewares/asyncWrapper');
const delay = require('../../utils/delay');

const { Op } = Sequelize;
const router = new Router();

router.get(
  '/',
  listQueryValidator,
  asyncWrapper(async (req, res) => {
    const { offset, limit, query, sort, minSalary, maxSalary } = req.query;
    const options = {
      offset,
      raw: true,
    };
    if (sort) {
      options.order = [[sort.slice(1), sort[0] === '-' ? 'desc' : 'asc']];
    }

    if (limit) {
      options.limit = limit;
    }

    const where = {};
    if (query) {
      where[Op.or] = [
        {
          name: {
            [Op.like]: `%${query}%`,
          },
        },
        {
          employeeId: {
            [Op.like]: `%${query}%`,
          },
        },
        {
          username: {
            [Op.like]: `%${query}%`,
          },
        },
      ];
    }

    if (minSalary != null && maxSalary != null) {
      set(where, 'salary', { [Op.gte]: minSalary, [Op.lte]: maxSalary });
    }

    options.where = where;

    const results = await User.findAndCountAll(options);
    results.meta = {};
    const [minSalaryResult, maxSalaryResult] = await Promise.all([
      User.getMinSalary(),
      User.getMaxSalary(),
    ]);
    results.meta.minSalary = parseFloat(minSalaryResult.minSalary);
    results.meta.maxSalary = parseFloat(maxSalaryResult.maxSalary);
    res.json(results);
  }),
);

router.get(
  '/:id',
  asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const user = await User.findByPk(id);
    if (!user) {
      res.status(404).send();
    }
    res.json(user);
  }),
);

router.post(
  '/',
  createBodyValidator,
  asyncWrapper(async (req, res) => {
    const { employeeId, username, name, salary } = req.body;
    const user = await User.create({
      employeeId,
      username,
      name,
      salary,
    });
    res.json(user);
  }),
);

router.post(
  '/upload',
  uploadBodyValidator,
  asyncWrapper(async (req, res) => {
    if (!req.files || !req.files.file) {
      res.status(400).json({
        message: 'File is required.',
      });
    }

    let lock = await Lock.findOne({
      where: {
        type: 'upload_users',
        status: 'pending',
      },
    });

    if (lock) {
      throw new Error(`${lock.name} is uploading users.`);
    }

    lock = await Lock.create({
      type: 'upload_users',
      status: 'pending',
      name: 'Meng Taing',
    });

    if (req.body.delay) {
      await delay(req.body.delay);
    }
    const filePath = get(req, 'files.file.tempFilePath');
    try {
      const items = [];
      const errors = [];
      await new Promise((resolve, reject) => {
        fs.createReadStream(filePath)
          .pipe(csv())
          .on('data', (row) => {
            const { value, error } = createBodyValidator.schema.validate(row);
            if (error) {
              errors.push({
                // eslint-disable-next-line no-underscore-dangle
                data: Object.values(error._original).join(','),
                message: error.details[0].message,
              });
            } else {
              items.push(value);
            }
          })
          .on('error', (error) => {
            reject(error);
          })
          .on('end', () => {
            resolve();
          });
      });
      if (errors.length) {
        res.status(400).json({
          message: Object.values(errors[0]).join('\n'),
          details: errors,
          count: errors.length,
        });
      } else if (items.length) {
        await sequelize.transaction((t) => {
          return User.bulkCreate(items, {
            transaction: t,
          });
        });
        res.json({
          count: items.length,
        });
      } else {
        res.status(400).json({
          message: 'No data was inserted.',
          count: 0,
        });
      }
    } finally {
      fs.unlink(filePath, () => {});
      lock.status = 'done';
      await lock.save();
    }
  }),
);

router.post(
  '/:id',
  updateBodyValidator,
  asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const { employeeId, username, name, salary } = req.body;
    const [count] = await User.update(
      {
        employeeId,
        username,
        name,
        salary,
      },
      { where: { id } },
    );
    if (count > 0) {
      res.json({});
    } else {
      res.status(404).send();
    }
  }),
);

router.delete(
  '/:id',
  asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const count = await User.destroy({ where: { id } });
    if (count > 0) {
      res.json({});
    } else {
      res.status(404).send();
    }
  }),
);

module.exports = router;
