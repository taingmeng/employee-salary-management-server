const request = require('supertest');
const app = require('../../app');
const truncate = require('../../../test/truncate');
const userFactory = require('../../../test/factories/user');

describe('/users', () => {
  beforeEach(async () => {
    await truncate();
  });

  describe('GET /users', () => {
    it('should return list of users when it is empty', async () => {
      const res = await request(app).get('/api/users');
      expect(res.status).toEqual(200);
      expect(res.body).toEqual({
        count: 0,
        meta: { maxSalary: null, minSalary: null },
        rows: [],
      });
    });

    it('should return list of users', async () => {
      await userFactory({
        employeeId: 'e001',
        username: 'johndoe',
        name: 'John Doe',
        salary: 1234.56,
      });
      await userFactory({
        employeeId: 'e002',
        username: 'janedoe',
        name: 'Jane Doe',
        salary: 5678.99,
      });
      const res = await request(app).get('/api/users');
      expect(res.status).toEqual(200);
      expect(res.body).toEqual({
        count: 2,
        meta: { maxSalary: 5678.99, minSalary: 1234.56 },
        rows: [
          {
            createdAt: expect.any(String),
            deletedAt: null,
            employeeId: 'e001',
            id: 1,
            name: 'John Doe',
            salary: 1234.56,
            updatedAt: expect.any(String),
            username: 'johndoe',
          },
          {
            createdAt: expect.any(String),
            deletedAt: null,
            employeeId: 'e002',
            id: 2,
            name: 'Jane Doe',
            salary: 5678.99,
            updatedAt: expect.any(String),
            username: 'janedoe',
          },
        ],
      });
    });
  });

  describe('POST /users', () => {
    it('should create a new user and return created user', async () => {
      const res = await request(app).post('/api/users').send({
        employeeId: 'e001',
        name: 'John Snow',
        username: 'johnsnow',
        salary: 1234.56,
      });

      expect(res.status).toEqual(200);
      expect(res.body).toEqual({
        createdAt: expect.any(String),
        employeeId: 'e001',
        id: 1,
        name: 'John Snow',
        salary: 1234.56,
        updatedAt: expect.any(String),
        username: 'johnsnow',
      });
    });

    it('should return error if employeeId is duplicated', async () => {
      await request(app).post('/api/users').send({
        employeeId: 'e001',
        name: 'John Snow',
        username: 'johnsnow',
        salary: 1234.56,
      });

      const res = await request(app).post('/api/users').send({
        employeeId: 'e001',
        name: 'Jane Snow',
        username: 'janesnow',
        salary: 1234.56,
      });

      expect(res.status).toEqual(400);
      expect(res.body.message).toEqual(
        "Users.employeeId must be unique 'e001'",
      );
    });
  });
});
