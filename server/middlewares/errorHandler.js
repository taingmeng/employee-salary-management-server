module.exports = (err, req, res, next) => {
  let details = err;
  let { message } = err;
  let status = 500;
  if (err.name.startsWith('Sequelize')) {
    details = err.errors;
    if (err.errors) {
      message = err.errors[0].message;
      if (err.errors[0].value) {
        message += ` '${err.errors[0].value}'`;
      }
    }
    status = 400;
  } else if (err.name === 'ValidationError') {
    status = 400;
  }
  res.status(status).json({
    message,
    details,
  });
};
