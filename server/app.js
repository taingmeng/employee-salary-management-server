const express = require('express');
const compression = require('compression');
const helmet = require('helmet');
const morgan = require('morgan');
const responseTime = require('response-time');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const routes = require('./routes');
const errorHandler = require('./middlewares/errorHandler');
require('./models');

const app = express();

app.use(compression());
app.use(helmet());
app.use(cors({ origin: '*' }));
app.use(bodyParser.json());
app.use(
  fileUpload({
    limits: { fileSize: 10 * 1024 * 1024 },
    useTempFiles: true,
    tempFileDir: 'tmp/',
  }),
);

app.use(morgan('tiny'));
app.use(routes);
app.use(errorHandler);

app.use(
  responseTime((_req, res, time) => {
    res.setHeader('X-Response-Time', `${time.toFixed(2)}ms`);
    res.setHeader('Server-Timing', `renderServerSideApp;dur=${time}`);
  }),
);

module.exports = app;
