module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Locks', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        type: {
          type: Sequelize.STRING,
        },
        name: {
          type: Sequelize.STRING,
        },
        status: {
          type: Sequelize.STRING,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        deletedAt: {
          type: Sequelize.DATE,
        },
      })
      .then(() => queryInterface.addIndex('Locks', ['status', 'type']));
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('Locks');
  },
};
