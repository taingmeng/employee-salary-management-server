module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Users', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        employeeId: {
          type: Sequelize.STRING,
          unique: true,
        },
        username: {
          type: Sequelize.STRING,
          unique: true,
        },
        name: {
          type: Sequelize.STRING,
        },
        salary: {
          type: Sequelize.DECIMAL(7, 2),
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        deletedAt: {
          type: Sequelize.DATE,
        },
      })
      .then(() =>
        queryInterface.addIndex('Users', ['employeeId', 'username', 'name']),
      );
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('Users');
  },
};
