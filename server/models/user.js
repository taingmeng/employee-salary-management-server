module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      employeeId: {
        type: DataTypes.STRING,
        unique: true,
      },
      username: {
        type: DataTypes.STRING,
        unique: true,
      },
      name: DataTypes.STRING,
      salary: DataTypes.DECIMAL(7, 2),
    },
    {
      paranoid: true,
      timestamps: true,
    },
  );
  User.getMaxSalary = () =>
    User.findOne({
      attributes: [[sequelize.fn('max', sequelize.col('salary')), 'maxSalary']],
      raw: true,
    });
  User.getMinSalary = () =>
    User.findOne({
      attributes: [[sequelize.fn('min', sequelize.col('salary')), 'minSalary']],
      raw: true,
    });
  return User;
};
