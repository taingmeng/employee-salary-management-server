module.exports = (sequelize, DataTypes) => {
  const Lock = sequelize.define('Lock', {
    type: {
      type: DataTypes.STRING,
    },
    name: {
      type: DataTypes.STRING,
    },
    status: {
      type: DataTypes.STRING,
    },
  });
  return Lock;
};
