# Employee Salary Management (NodeJS)

Web application to browse employee salaries and perform other CMS operations.

## How to run locally

`npm run docker` starts server and database in Docker containers. This sets `process.env.NODE_ENV = 'docker'`.

If you want to run server alone out of container, you can run `npm start`.

## Scripts

- `npm start`: start server in development using nodemon. When the app crashes, it should restart itself automatically.
- `npm test`: run tests in watch mode.
- `npm run docker`: run in docker.
- `npm run jest`: run tests in with Jest.
- `npm run coverage`: run tests with coverage report at the end.
- `npm run lint`: lint codes with eslint.
- `npm run db:create`: create database.
- `npm run db:drop`: drop database.
- `npm run db:migrate`: run db migration files in `server/migrations`.

## Test

- Jest is used as the testing framework.
- When you run tests, make sure to run `npm run docker` to keep database running. API tests requires actual connection to database.

## Sequelize

- Sequelize is used as the database ORM.
- Model is generated along with migration file. For example:
  ```
  npx sequelize-cli model:generate --name User --attributes employeeId:string,username:string,name:string,salary:decimal --paranoid
  ```

## API

API endpoints start with `{{host}}/api`.

### Users

- [List](documentation/api/users/list.md): `GET /users`
- [Get](documentation/api/users/get.md): `GET /users/:id`
- [Create](documentation/api/users/creat.md): `POST /users`
- [Upload](documentation/api/users/upload.md): `POST /users/upload`
- [Update](documentation/api/users/update.md): `POST /users/:id`
- [Delete](documentation/api/users/delete.md): `DELETE /users/:id`
