/* eslint-disable no-console */
const chalk = require('chalk');
const express = require('express');
const {
  choosePort,
  prepareUrls,
} = require('react-dev-utils/WebpackDevServerUtils');

process.on('unhandledRejection', (err) => {
  throw err;
});

const DEFAULT_PORT = process.env.PORT || 5000;
const HOST = process.env.HOST || '0.0.0.0';
const server = express();

server.use((req, res) => {
  // eslint-disable-next-line global-require
  const app = require('../server/app');
  app(req, res);
});

choosePort(HOST, DEFAULT_PORT).then((port) => {
  if (!port) {
    return;
  }

  const urls = prepareUrls('http', HOST, port);

  server.listen(port, HOST, (err) => {
    if (err) {
      console.log(err);
      return;
    }

    console.log(chalk.white('\n\tStarting dev server...'));

    console.log(
      chalk.blue(`
        Running locally at ${urls.localUrlForBrowser}
        Running on your network at ${urls.lanUrlForConfig}:${port}
      `),
    );
  });
});
